package com.`fun`.weather.modules.main

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.`fun`.weather.R
import com.`fun`.weather.configs.GenericHelper
import com.`fun`.weather.configs.IWeatherInfo
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), IMainView {

    private val dateFormat = SimpleDateFormat("HH:mm, dd.MM.yyyy", Locale.ENGLISH)
    private lateinit var presenter: MainPresenter

    @SuppressLint("SetTextI18n")
    override fun drawWeather(weatherInfo: IWeatherInfo) {
        progress_bar.visibility = GONE
        header_tv.text = getString(R.string.location) + " ${weatherInfo.city()}"
        conditions_tv.text = "${weatherInfo.description()}".toUpperCase()
        temp_tv.text = getString(R.string.temperature) + " ${weatherInfo.temp()}"
        wind_tv.text = getString(R.string.wind_speed) + " ${weatherInfo.wind()}"
        time_updated_tv.text = getString(R.string.last_update) + " ${dateFormat.format(System.currentTimeMillis())}"
        val iconId = weatherInfo.img()
        if (iconId != null) {
            Glide.with(this).load(GenericHelper.buildImgUrl(iconId)).into(weather_iv)
        } else {
            weather_iv.setImageDrawable(null)
        }
    }

    override fun drawError(text: CharSequence) {
        progress_bar.visibility = GONE
        Toast.makeText(this.applicationContext, "Error: $text", Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = ViewModelProviders.of(this).get(MainPresenter::class.java)
        presenter.bind(this)
        loadWeather()
    }

    private fun loadWeather(){
        progress_bar.visibility = VISIBLE
        presenter.loadCurrentWeather()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbind()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.refresh_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.refreshItem -> {
                loadWeather()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
