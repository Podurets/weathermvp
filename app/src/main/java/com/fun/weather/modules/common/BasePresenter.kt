package com.`fun`.weather.modules.common

import androidx.lifecycle.ViewModel

abstract class BasePresenter<V : IView> : ViewModel() {

    private var view : V? = null

    fun bind(view : V){
       this.view = view
    }

    fun unbind(){
        view = null
    }

    fun view() = view

    override fun onCleared() {
        super.onCleared()
        view = null
    }

}