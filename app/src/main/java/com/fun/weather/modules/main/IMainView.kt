package com.`fun`.weather.modules.main

import com.`fun`.weather.configs.IWeatherInfo
import com.`fun`.weather.modules.common.IView

interface IMainView : IView {

    fun drawError(text : CharSequence)
    fun drawWeather(weatherInfo: IWeatherInfo)

}