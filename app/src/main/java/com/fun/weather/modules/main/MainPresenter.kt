package com.`fun`.weather.modules.main

import com.`fun`.weather.configs.AppRepository
import com.`fun`.weather.configs.Constants
import com.`fun`.weather.configs.IRepository
import com.`fun`.weather.configs.IWeatherInfo
import com.`fun`.weather.modules.common.BasePresenter

open class MainPresenter : BasePresenter<IMainView>(), IMainPresenter {

    override fun loadCurrentWeather() {
        AppRepository.loadCurrentWeather(
            Constants.LANG,
            Constants.CITY,
            Constants.COUNTRY,
            object : IRepository.ICallback<IWeatherInfo> {
                override fun onSuccess(data: IWeatherInfo) {
                    onSuccessLoad(data)
                }

                override fun onError(error: String) {
                    onErrorLoad(error)
                }
            })
    }

    fun onSuccessLoad(data: IWeatherInfo) {
        val view = view()
        view?.drawWeather(data)
    }

    fun onErrorLoad(error: String) {
        val view = view()
        view?.drawError(error)
    }
}