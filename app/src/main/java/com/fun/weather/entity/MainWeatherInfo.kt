package com.`fun`.weather.entity

import com.google.gson.annotations.SerializedName

class MainWeatherInfo {

    @SerializedName("temp")
    var temp : Float = 0f
    @SerializedName("pressure")
    var pressure : Int = 0
    @SerializedName("humidity")
    var humidity : Int = 0
    @SerializedName("temp_min")
    var tempMin : Float = 0f
    @SerializedName("temp_max")
    var tempMax : Float = 0f

}