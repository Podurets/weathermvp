package com.`fun`.weather.entity

import com.`fun`.weather.configs.IWeatherInfo
import com.google.gson.annotations.SerializedName

open class ForecastInfo : IWeatherInfo {

    override fun description(): String? {
        if (weather != null && weather?.size!! > 0) {
            return weather?.get(0)?.description ?: weather?.get(0)?.main
        }
        return null
    }

    override fun temp() = mainWeatherInfo?.temp ?: 0f

    override fun wind() = wind?.speed ?: 0f

    override fun city() = name

    override fun img(): String? {
        if (weather != null && weather?.size!! > 0) {
            return weather?.get(0)?.icon
        }
        return null
    }

    @SerializedName("weather")
    var weather: List<Weather>? = null
    @SerializedName("main")
    var mainWeatherInfo: MainWeatherInfo? = null
    @SerializedName("wind")
    var wind: WindInfo? = null
    @SerializedName("name")
    var name: String? = null

}