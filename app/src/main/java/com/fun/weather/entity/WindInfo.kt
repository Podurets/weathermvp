package com.`fun`.weather.entity

import com.google.gson.annotations.SerializedName

class WindInfo {

    @SerializedName("speed")
    var speed: Float = 0f
    @SerializedName("deg")
    var deg: Float = 0f
}