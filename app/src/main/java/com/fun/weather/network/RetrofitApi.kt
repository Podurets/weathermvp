package com.`fun`.weather.network

import com.`fun`.weather.configs.Constants
import com.`fun`.weather.entity.ForecastInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApi {

    @GET("data/2.5/weather?units=metric&appid=" + Constants.API_KEY)
    fun loadCurrentWeather(
        @Query("lang") lang: String, @Query("lat") lat: Float,
        @Query("lon") lon: Float
    ): Call<ForecastInfo>

    @GET("data/2.5/weather?units=metric&appid=" + Constants.API_KEY)
    fun loadCurrentWeather(
        @Query("lang") lang: String,
        @Query(value = "q", encoded = true) cityCountry: String
    ): Call<ForecastInfo>



}