package com.`fun`.weather.network

import com.`fun`.weather.configs.IRepository
import com.`fun`.weather.configs.IWeatherInfo
import com.`fun`.weather.entity.ForecastInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitRepository : IRepository {

    private val retrofitApi = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("http://api.openweathermap.org/")
        .build().create(RetrofitApi::class.java)


    private fun makeCurrentWeatherCall(call: Call<ForecastInfo>, callback: IRepository.ICallback<IWeatherInfo>) {
        call.enqueue(object : Callback<ForecastInfo> {

            override fun onFailure(call: Call<ForecastInfo>, t: Throwable) {
                callback.onError(t.message ?: t.toString())
            }

            override fun onResponse(call: Call<ForecastInfo>, response: Response<ForecastInfo>) {
                if (response.isSuccessful) {
                    val weatherInfo = response.body()
                    if (weatherInfo != null) {
                        callback.onSuccess(weatherInfo)
                    } else {
                        callback.onError("Parse error")
                    }
                } else {
                    callback.onError(response.message())
                }
            }

        })
    }

    override fun loadCurrentWeather(
        lang: String,
        lat: Float,
        lon: Float,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {
        makeCurrentWeatherCall(retrofitApi.loadCurrentWeather(lang, lat, lon), callback)
    }

    override fun loadCurrentWeather(
        lang: String,
        city: String,
        country: String,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {
        makeCurrentWeatherCall(retrofitApi.loadCurrentWeather(lang, "$city,$country"), callback)
    }


}