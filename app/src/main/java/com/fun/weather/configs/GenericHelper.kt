package com.`fun`.weather.configs

object GenericHelper {

    fun buildImgUrl(iconId: String) = "http://openweathermap.org/img/wn/$iconId@2x.png"

    fun toFahrenheit(celcius: Double) = (celcius * 1.8) + 32

    fun toCelcius(fahreiheit: Double) = (fahreiheit - 32) / 1.8

}