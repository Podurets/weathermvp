package com.`fun`.weather.configs

interface IWeatherInfo {

    fun temp(): Float
    fun wind(): Float
    fun city(): String?
    fun img(): String?
    fun description(): String?

}