package com.`fun`.weather.configs

import com.`fun`.weather.network.RetrofitRepository

object AppRepository : IRepository {

    private var networkRepository : IRepository = RetrofitRepository

    override fun loadCurrentWeather(
        lang: String,
        lat: Float,
        lon: Float,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {
        networkRepository.loadCurrentWeather(lang, lat, lon, callback)
    }

    override fun loadCurrentWeather(
        lang: String,
        city: String,
        country: String,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {
        networkRepository.loadCurrentWeather(lang, city, country, callback)
    }



}