package com.`fun`.weather.configs

interface IRepository {

    fun loadCurrentWeather(lang: String, lat: Float, lon: Float, callback: ICallback<IWeatherInfo>)
    fun loadCurrentWeather(lang: String, city: String, country: String, callback: ICallback<IWeatherInfo>)


    interface ICallback<Data> {

        fun onSuccess(data: Data)
        fun onError(error: String)
    }

}