package com.`fun`.weather

import android.content.Intent
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.PerformException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.util.HumanReadables
import androidx.test.espresso.util.TreeIterables
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.`fun`.weather.configs.Constants
import com.`fun`.weather.modules.main.MainActivity
import junit.framework.AssertionFailedError
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeoutException


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    private val firstActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)
    private var mainActivity: MainActivity? = null

    @Before
    fun checkStartActivity() {
        if (mainActivity == null) {
            mainActivity = firstActivityRule.launchActivity(Intent())
        } else {
            try {
                intended(hasComponent(MainActivity::class.java.name))
            } catch (e: AssertionFailedError) {
                mainActivity = firstActivityRule.launchActivity(Intent())
            }
        }
    }

    @After
    fun releaseData() {
        firstActivityRule.finishActivity()
    }

    @Test
    fun useAppContext() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.fun.weather", context.packageName)
    }

    @Test
    fun testUILoadWeatherOnStart() {
        waitAndCheckServersResult()
    }

    @Test
    fun reloadWeather() {
        onView(withId(R.id.refreshItem)).perform(click())
        waitAndCheckServersResult()
    }

    private fun waitAndCheckServersResult() {
        onView(isRoot()).perform(waitFor(Constants.TIMEOUT))
        onView(withId(R.id.conditions_tv)).check(matches(not(withText(""))))
    }


    private fun waitFor(millis: Long): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): org.hamcrest.Matcher<View>? {
                return isRoot()
            }

            override fun getDescription(): String {
                return "Wait for $millis milliseconds."
            }

            override fun perform(uiController: UiController, view: View) {
                uiController.loopMainThreadForAtLeast(millis)
            }
        }
    }
}
