package com.`fun`.weather

import com.`fun`.weather.configs.IRepository
import com.`fun`.weather.configs.IWeatherInfo

class MockRepository : IRepository {
    override fun loadCurrentWeather(
        lang: String,
        city: String,
        country: String,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {

    }


    override fun loadCurrentWeather(
        lang: String,
        lat: Float,
        lon: Float,
        callback: IRepository.ICallback<IWeatherInfo>
    ) {

    }


    fun currentWeatherJson(): String {
        return "{\"coord\":{\"lon\":30.52,\"lat\":50.45},\"weather\":[{\"id\":520,\"main\":\"Rain\",\"description\":\"дощ\",\"icon\":\"09d\"}],\"base\":\"stations\",\"main\":{\"temp\":18.78,\"pressure\":1010,\"humidity\":88,\"temp_min\":17,\"temp_max\":21},\"visibility\":10000,\"wind\":{\"speed\":3,\"deg\":320},\"clouds\":{\"all\":20},\"dt\":1563195276,\"sys\":{\"type\":1,\"id\":8903,\"message\":0.0175,\"country\":\"UA\",\"sunrise\":1563156162,\"sunset\":1563213873},\"timezone\":10800,\"id\":703448,\"name\":\"Kyiv\",\"cod\":200}"
    }

}