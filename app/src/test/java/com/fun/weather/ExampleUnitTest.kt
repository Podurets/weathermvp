package com.`fun`.weather

import com.`fun`.weather.configs.GenericHelper
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun temperatureConvertTest() {
        assertEquals(GenericHelper.toFahrenheit(20.0), 68.0, 0.01)
        assertEquals(GenericHelper.toCelcius(68.0), 20.0, 0.01)
    }
}
