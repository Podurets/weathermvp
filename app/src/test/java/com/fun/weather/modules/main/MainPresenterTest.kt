package com.`fun`.weather.modules.main

import com.`fun`.weather.MockRepository
import com.`fun`.weather.entity.ForecastInfo
import com.google.gson.Gson
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.validateMockitoUsage
import org.mockito.MockitoAnnotations


@RunWith(org.mockito.junit.MockitoJUnitRunner::class)
class MainPresenterTest {

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun validate() {
        validateMockitoUsage()
    }

    @Test
    fun parseCurrentWeather() {
        val repo = MockRepository()
        val json = repo.currentWeatherJson()
        val forecast = Gson().fromJson(json, ForecastInfo::class.java)
        assertNotNull(forecast)
        assertTrue(forecast.weather != null)
        assertTrue(forecast.name != null)
        assertTrue(forecast.mainWeatherInfo != null)
        assertTrue(forecast.wind != null)
    }

}